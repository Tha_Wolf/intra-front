import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {PostsService} from '../services/posts.service';

export interface Publicacion {
  id: number;
  title: string;
  img:string;
  text: string;
  html: string;
}

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.css']
})
export class PostPageComponent implements OnInit {
  publicaciones: Publicacion[] = [/*
    {id:1,img:'assets/img/pub1.png',title:'Búsqueda Interna',text: 'Participamos a los empleados de Aguas Bonaerenses S.A. en nuestra búsqueda laboral para cubrir el puesto de  ANALISTA en el sector CATASTRO para desarrollar tareas en el EDIFICIO CENTRAL.',html:'<div><b>This text is bold</b> and this one is <i>italics</i></div>'},
    {id:2,img:'assets/img/pub2.png',title:'1° MAYO-DIA DEL TRABAJADOR',text: 'Feliz Dia Del Trabajador para todos los que trabajan día a día para mejorar el servicio de agua potable y cloacas de cada región.',html:'<div><b>This text is bold</b> and this one is <i>italics</i></div>'},
    {id:3,img:'assets/img/pub3.png',title:'Actualización Aplicativo JAVA',text: 'Con el objetivo de mejorar la calidad y tiempo de atencion, la simplificacion del trabajo para el operador y la apertura a nuevas tecnilogias, se realilzo la actualizacion y mejora de la version del aplicativo comercial JAVA y unificacion del entorno 4GL.',html:'<div><b>This text is bold</b> and this one is <i>italics</i></div>'},
*/
  ];
  constructor(private route: ActivatedRoute, private Posts: PostsService) {}

  publicacion: Publicacion = this.publicaciones[1];

  ngOnInit() {
    this.Posts.getpublicaciones().subscribe(data => data.forEach((p: Publicacion) => {
        if (p.id == this.route.snapshot.params.id) {
          this.publicacion = p;
        }
      }) );
    console.log(this.publicaciones);

    /*this.publicaciones.forEach((p: Publicacion) => {
        if (p.id == this.route.snapshot.params.id) {
          this.publicacion = p;
        }
      });*/
  }

}
