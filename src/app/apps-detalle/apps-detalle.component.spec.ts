import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsDetalleComponent } from './apps-detalle.component';

describe('AppsDetalleComponent', () => {
  let component: AppsDetalleComponent;
  let fixture: ComponentFixture<AppsDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppsDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
