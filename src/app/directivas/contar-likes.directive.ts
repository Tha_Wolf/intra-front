import {  Directive, Output, EventEmitter, Input, SimpleChange, OnInit } from '@angular/core';
import {LikesService} from '../services/likes.service';

@Directive({
  selector: '[appContarLikes]'
})
export class ContarLikesDirective implements OnInit {


  @Output() onCreate: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }
  ngOnInit() {
     this.onCreate.emit('dummy');
  }


}
