import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './material.module';
import {AppRoutingModule, RoutingComponents } from './approuting.module';

import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ThumbComponent } from './thumb/thumb.component';
import {Publicaciones} from './post/post.component';
import { FooterComponent } from './footer/footer.component';
import { PostPageComponent } from './post-page/post-page.component';
import { AppsListaComponent } from './apps-lista/apps-lista.component';
import { AppsDetalleComponent } from './apps-detalle/apps-detalle.component';
import { NormalizacionComponent } from './normalizacion/normalizacion.component';
import {HttpClientModule} from '@angular/common/http';
import {DirectorioTelefonicoService} from './services/directoriotelefonico.service';
import {LoginService} from './services/login.service';
import {TokenService} from './services/token.service';
import {AuthService} from './services/Auth.service';
import { FiltroPipe } from './filtro.pipe';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import{LikesService} from './services/likes.service';
import { ContarLikesDirective } from './directivas/contar-likes.directive';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ThumbComponent,
    RoutingComponents,
    Publicaciones,
    FooterComponent,
    PostPageComponent,
    AppsListaComponent,
    AppsDetalleComponent,
    NormalizacionComponent,
    FiltroPipe,
    LoginComponent,
    ContarLikesDirective,



  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,  
    MaterialModule,
    LayoutModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,




  ],
  providers: [DirectorioTelefonicoService,LoginService,TokenService,AuthService,LikesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
