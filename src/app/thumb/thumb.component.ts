import { Component, OnInit } from '@angular/core';

export interface Thumb {
  img: string;
  text: string;
  cols: number;
  rows: number;
  link: string;
}

@Component({
  selector: 'app-thumb',
  templateUrl: './thumb.component.html',
  styleUrls: ['./thumb.component.css']
})



export class ThumbComponent {
  thumbs: Thumb[] = [
    {img: 'assets/img/tyr.png', text: 'TYRII',cols: 1, rows: 2, link:"http://172.27.104.193/tyr/main.aspx"},
    {img: 'assets/img/ot.jfif', text: 'OT',cols: 1, rows: 2,link:"http://172.27.104.193/ot/main.aspx"},
    {img: 'assets/img/sap.png', text: 'SAP',cols: 1, rows: 2,link:"https://ec2-34-226-234-87.compute-1.amazonaws.com:44300/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html?sap-client=400&sap-language=ES#Shell-home"},
    {img: 'assets/img/capacitacion.jfif', text: 'CAPACITACION',cols: 1, rows: 2,link:"http://capacitacion.aguasbonaerenses.com.ar"},
    {img: 'assets/img/qliksense.jfif', text: 'QLIK',cols: 1, rows: 2,link:"http://"},
    {img: 'assets/img/raet.jfif', text: 'RAET',cols: 1, rows: 2,link:"http://"},
    //{img: 'assets/img/outlook.jpg', text: 'OUTLOOK',cols: 1, rows: 2,link:"http://correo.aguasbonaerenses.com.ar"},
    //{img: 'assets/img/logoai.png', text: 'MÁS APLICACIONES',cols: 1, rows: 2,link:"/appslista"},
  ];

  externalUrl(link:string){
    window.open(link);
  }
}
