import { Component,OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material';
import {MatTableDataSource} from '@angular/material';

export interface DocNormalizacion {
  nombre: string;
  tipo: string;
  gerencia: string;
  revision: string;
  fecha: string;
  descripcion: string;
  link: string;
}

export class Group {
  level: number = 0;
  parent: Group;
  expanded: boolean = true;
  get visible(): boolean {
    return !this.parent || (this.parent.visible && this.parent.expanded);
  }
}


const ELEMENT_DATA: DocNormalizacion[] = [
  {gerencia: 'Presidencia', tipo: 'Matriz', nombre: 'Matriz de Autoridad', revision: '03',fecha: '18/05/2018',descripcion: 'Matriz de aprobaciones de ABSA',link:'http://localhost:8000/public/doc-normalizacion/1- Presidencia/Matriz de Autoridad-V3.pdf'},
  {gerencia: 'Presidencia', tipo: 'Reglamento General', nombre: 'Reglamento General de Contrataciones', revision: '',fecha: '',descripcion: 'Reglamentación general para compras y contrataciones en ABSA',link:''},
  {gerencia: 'Normalizacion y Gestión de Calidad', tipo: 'Norma General', nombre: 'NG-001 Gestión de Documentos Normativos', revision: '00',fecha: '04/09/2015',descripcion: 'Metodología para la elaboración, revisión, aprobación y control de los documentos normativos en ABSA',link:''},
  {gerencia: 'Gerencia de Administracion y Finanzas', tipo: 'Norma General', nombre: 'NG-002 Gastos de Traslado y Viaticos', revision: '01',fecha: '24/06/2019',descripcion: 'Lineamientos para realizar solicitudes de anticipos y rendiciones de gastos de traslado y viáticos por motivos laborales en ABSA',link:''},
  {gerencia: 'Gerencia de Administracion', tipo: 'Norma Especifica', nombre: 'NE-GSI-001 Marco Normativo de IT', revision: '00',fecha: '17/07/2018',descripcion: 'Lineamientos para las actividades relacionadas con la utilizacion de recursos de tecnologia de la informacion y de las comunicaciones',link:''},
  {gerencia: 'Gerencia de Administracion', tipo: 'Procedimiento', nombre: 'PR-DRH-001 Uso y Administracion de Vehiculos', revision: '00',fecha: '31/10/2016',descripcion: 'Metodologia de asignacion, utilizacion y administracion del parque automotor de ABSA',link:''},
  {gerencia: 'Gerencia de Administracion', tipo: 'Instructivo', nombre: 'IT-GAD-001 Denuncia por Siniestro de Vehiculos', revision: '00',fecha: '03/05/2017',descripcion: 'Actividades a realizar en caso de siniestros de vehiculos',link:''},
  {gerencia: 'Gerencia de Administracion', tipo: 'Instructivo', nombre: 'IT-GAD-002 Registro de Correpondencia en Mesa de Entradas', revision: '02',fecha: '21/02/2018',descripcion: 'Metodologia para el registro de la correspondencia mediante el Sistema TYR de Mesa de Entradas de ABSA',link:''},
  {gerencia: 'Gerencia de Administracion', tipo: 'Instructivo', nombre: 'IT-GAD-003 Infracciones de Transito', revision: '00',fecha: '19/04/2018',descripcion: 'Metodologia para el tratamiento integral de las infracciones de transito en vehiculos de ABSA',link:''},
  {gerencia: 'Gerencia de Administracion', tipo: 'Instructivo', nombre: 'IT-GAD-004 Utilizacion de la Tarjeta de Combustible', revision: '00',fecha: '29/08/2018',descripcion: 'Metodologia par ala utilizacion de la tarjeta de uso de combustible de ABSA',link:''},
  {gerencia: 'Gerencia de Administracion', tipo: 'Instructivo', nombre: 'IT-GAD-005 Tarjetas de Credito Corporativas', revision: '00',fecha: '29/08/2018',descripcion: 'Lineamientos para el uso de tarjetas de credito corporativas',link:''},
  {gerencia: 'Gerencia de Atención al Usuario y Gestión Comercial', tipo: 'Procedimiento', nombre: 'PR-GCO-002 - Gestión de Medidores', revision: '00',fecha: '04/08/2016',descripcion: 'Proceso de adquisición, asignación, verificación, recambio, control y desasignación de los Medidores en ABSA.',link:''},
  {gerencia: 'Gerencia de Atención al Usuario y Gestión Comercial', tipo: 'Procedimiento', nombre: 'PR-GCO-003 - Lecturas', revision: '00',fecha: '04/08/2016',descripcion: 'Proceso de Lecturas de los medidores registrados en el Sistema Comercial de ABSA.',link:''},
  {gerencia: 'Gerencia de Atención al Usuario y Gestión Comercial', tipo: 'Procedimiento', nombre: 'PR-GCO-004 - Consideración por Pérdida Interna no Visible y Bajo Piso', revision: '00',fecha: '31/07/2018',descripcion: 'Metodología para el tratamiento de las solicitudes de consideración por pérdida interna no visible y bajo piso en ABSA.',link:''},
  {gerencia: 'Gerencia de Asuntos Institucionales', tipo: 'Procedimiento', nombre: 'PR-DAI-001 - Comunicación del Estado del Servicio ', revision: '00',fecha: '18/08/2016',descripcion: 'Lineamientos para la recepción y la emisión de información oficial.',link:''},
  {gerencia: 'Dirección de Recursos Humanos', tipo: 'Procedimiento', nombre: 'PR-DRH-002 - Incorporación de Personal', revision: '00',fecha: '25/09/2017',descripcion: 'Metodologia a implementar para la incorporación de personal en ABSA.',link:''},
  {gerencia: 'Dirección de Ingeniería, Producción, Obras y Mantenimiento', tipo: 'Norma Específica', nombre: 'NE-GOP-001 - Cartografía Digital de Redes de Servicio', revision: '00',fecha: '18/10/2017',descripcion: 'Lineamientos para la creación, modificación y mantenimiento de la cartografía digital de las redes de servicios de agua y desagües cloacales en ABSA.',link:''},
  {gerencia: 'Gerencia de Ingeniería, Calidad y Gestión de Porcesos', tipo: 'Instructivo', nombre: 'IT-GIN-001 - Proyectos de Ingeniería', revision: '00',fecha: '18/03/2018',descripcion: 'Metodología para la solicitud y tratamiento de los Proyectos de Ingeniería en ABSA.',link:''},
];

@Component({
  selector: 'app-normalizacion',
  templateUrl: './normalizacion.component.html',
  styleUrls: ['./normalizacion.component.css']
})



export class NormalizacionComponent  implements OnInit{
    displayedColumns: string[] = ['gerencia', 'tipo', 'nombre', 'revision', 'fecha', 'descripcion','link'];
    dataSource = new MatTableDataSource<DocNormalizacion>(ELEMENT_DATA);

    //@ViewChild(MatPaginator) paginator: MatPaginator;



    groupByColumns: string[] = ['gerencia'];

    ngOnInit() {
        this.dataSource.data = this.addGroups(ELEMENT_DATA, this.groupByColumns);
        this.dataSource.filterPredicate = this.customFilterPredicate.bind(this);
      }



  customFilterPredicate(data: DocNormalizacion | Group, filter: string): boolean {
    return (data instanceof Group) ? data.visible : this.getDataRowVisible(data);
  }

  getDataRowVisible(data: DocNormalizacion): boolean {
    const groupRows = this.dataSource.data.filter(
      row => {
        if (!(row instanceof Group)) return false;

        let match = true;
        this.groupByColumns.forEach(
          column => {
            if (!row[column] || !data[column] || row[column] !== data[column]) match = false;
          }
        );
        return match;
      }
    );
    if (groupRows.length === 0) return true;
    if (groupRows.length > 1) throw "Data row is in more than one group!";
    const parent = <Group><unknown>groupRows[0];  // </Group> (Fix syntax coloring)

    return parent.visible && parent.expanded;
  }

  groupHeaderClick(row) {
    row.expanded = !row.expanded
    this.dataSource.filter = performance.now().toString();  // hack to trigger filter refresh
  }

  addGroups(data: any[], groupByColumns: string[]): any[] {
    var rootGroup = new Group();
    return this.getSublevel(data, 0, groupByColumns, rootGroup);
  }

  getSublevel(data: any[], level: number, groupByColumns: string[], parent: Group): any[] {
    // Recursive function, stop when there are no more levels.
    if (level >= groupByColumns.length)
      return data;

    var groups = this.uniqueBy(
      data.map(
        row => {
          var result = new Group();
          result.level = level + 1;
          result.parent = parent;
          for (var i = 0; i <= level; i++)
            result[groupByColumns[i]] = row[groupByColumns[i]];
          return result;
        }
      ),
      JSON.stringify);

    const currentColumn = groupByColumns[level];

    var subGroups = [];
    groups.forEach(group => {
      let rowsInGroup = data.filter(row => group[currentColumn] === row[currentColumn])
      let subGroup = this.getSublevel(rowsInGroup, level + 1, groupByColumns, group);
      subGroup.unshift(group);
      subGroups = subGroups.concat(subGroup);
    })
    return subGroups;
  }

  uniqueBy(a, key) {
    var seen = {};
    return a.filter(function (item) {
      var k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    })
  }

  isGroup(index, item): boolean {
    return item.level;
  }


  /*ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }*/

}
