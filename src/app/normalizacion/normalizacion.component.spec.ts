import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalizacionComponent } from './normalizacion.component';

describe('NormalizacionComponent', () => {
  let component: NormalizacionComponent;
  let fixture: ComponentFixture<NormalizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
