import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { DirectorioComponent } from './directorio/directorio.component';
import { HomeComponent } from './home/home.component';
import { PostPageComponent} from './post-page/post-page.component';
import { AppsListaComponent } from './apps-lista/apps-lista.component';
import { AppsDetalleComponent } from './apps-detalle/apps-detalle.component';
import {NormalizacionComponent} from './normalizacion/normalizacion.component';
import {LoginComponent} from './login/login.component';
import {Publicaciones} from './post/post.component';


const routes: Routes = [
{ path: 'directorio', component: DirectorioComponent},
{ path: '', component : HomeComponent},
{ path: 'home', component : HomeComponent},
{ path: 'publicacion/:id', component : PostPageComponent},
{ path: 'postslista', component : Publicaciones},
{ path: 'appslista', component : AppsListaComponent},
{ path: 'appdetalle/:id', component : AppsDetalleComponent},
{ path: 'normalizacion', component: NormalizacionComponent},
{path: 'login', component: LoginComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
export const RoutingComponents = [DirectorioComponent, HomeComponent]
