import { Component,OnInit, ViewChild,OnChanges }from '@angular/core';
import {MatPaginator} from '@angular/material';
import {MatTableDataSource} from '@angular/material';
import {DirectorioTelefonicoService} from '../services/directoriotelefonico.service';

export interface DirectorioTelefonico {
  id: number;
  nombre: string;
  posicion: string;
  gerencia: string;
  edificio: string;
  interno: string;
}



/*let ELEMENT_DATA: DirectorioTelefonico[]; = [
  {nombre: 'Amado Sandra', posicion: 'Analista de sistemas', gerencia: 'Gerencia de sistemas', edificio: 'ADM-L.P.CALLE 56 NRO.534',interno: '4297977'},
  {nombre: 'Ana Paula Bascougnet', posicion: 'Analista Administracion de personal', gerencia: 'Gerencia de Recursos Humanos', edificio: 'ADM-LP CALLE 43 ESQ.7',interno: '5127968'},
  {nombre: 'Bailleau Sebastian', posicion: 'Jefe aplicaciones informaticas', gerencia: 'Gerencia de sistemas', edificio: 'ADM-L.P.CALLE 56 NRO.534',interno: '4297528'},
  {nombre: 'Barboni Jorge', posicion: 'Analista Administracion de personal', gerencia: 'Gerencia de Recursos Humanos', edificio: 'ADM-LP CALLE 43 ESQ.7',interno: '5126814'},
  {nombre: 'Berizonce Eugenia', posicion: 'Analista Técnico - Sistemas', gerencia: 'Gerencia de sistemas', edificio: 'ADM-L.P.CALLE 56 NRO.534',interno: '4297976'},
  {nombre: 'Cañueto Sebastian', posicion: 'Analista Programador', gerencia: 'Gerencia de sistemas', edificio: 'ADM-L.P.CALLE 56 NRO.534',interno: '4297528'},
  {nombre: 'Gallay Mariano', posicion: 'Jefe Centro de Atencion Informática', gerencia: 'Gerencia de sistemas', edificio: 'ADM-L.P.CALLE 56 NRO.534',interno: '4297932'},
  {nombre: 'Gonzalez Ludovico', posicion: 'Soporte CAI', gerencia: 'Gerencia de sistemas', edificio: 'ADM-L.P.CALLE 56 NRO.534',interno: '4297923'},
  {nombre: 'Pereyra Hector Javier', posicion: 'Analista Administracion de personal', gerencia: 'Gerencia de Recursos Humanos', edificio: 'ADM-LP CALLE 43 ESQ.7',interno: '5126821'},
  {nombre: 'Piccioni Sebastian', posicion: 'Analista de sistemas', gerencia: 'Gerencia de sistemas', edificio: 'ADM-L.P.CALLE 56 NRO.534',interno: '4297572'},
];*/

@Component({
  selector: 'app-directorio',
  templateUrl: './directorio.component.html',
  styleUrls: ['./directorio.component.css']
})



export class DirectorioComponent  implements OnInit{

  public telefonos = [];

  constructor (private _directoriotelefonicoservice: DirectorioTelefonicoService){}

  displayedColumns: string[] = ['nombre', 'posicion', 'gerencia', 'edificio', 'interno'];
  dataSource = new MatTableDataSource<DirectorioTelefonico>(this.telefonos);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  public doFilter = (value: string) => {
      this.dataSource.filter = value.trim().toLocaleLowerCase();
    }


  ngOnInit() {
    this.setData();
this.dataSource.paginator = this.paginator;


  }

setData() {
  this._directoriotelefonicoservice.gettelefonos().subscribe(data => this.dataSource = new MatTableDataSource<DirectorioTelefonico>(data),

  );

}









}
