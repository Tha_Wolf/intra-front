import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppsListaComponent } from './apps-lista.component';

describe('AppsListaComponent', () => {
  let component: AppsListaComponent;
  let fixture: ComponentFixture<AppsListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppsListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppsListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
