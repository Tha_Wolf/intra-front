import { Component, OnInit } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {Observable} from 'rxjs';
import {AppsService} from '../services/apps.service';

export interface App {
  id:number;
  img: string;
  text: string;
  subtext:string;
  body:string;
  cols: number;
  rows: number;
  link: string;
}

@Component({
  selector: 'app-apps-lista',
  templateUrl: './apps-lista.component.html',
  styleUrls: ['./apps-lista.component.css'],
})

export class AppsListaComponent implements OnInit{
  searchText: string = "";
  apps: App[] = [];
    /*
    {id: 0,img: 'assets/img/tyr.png', text: 'TYRII',subtext:'',body:'',cols: 1, rows: 2, link:"http://172.27.104.193/tyr/main.aspx"},
    {id: 1,img: 'assets/img/ot.jfif', text: 'OT',subtext:'',body:'',cols: 1, rows: 2,link:"http://172.27.104.193/ot/main.aspx"},
    {id: 2,img: 'assets/img/sap.png', text: 'SAP',subtext:'',body:'',cols: 1, rows: 2,link:"https://ec2-34-226-234-87.compute-1.amazonaws.com:44300/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html?sap-client=400&sap-language=ES#Shell-home"},
    {id: 3,img: 'assets/img/capacitacion.jfif', text: 'CAPACITACION',subtext:'',body:'',cols: 1, rows: 2,link:"http://capacitacion.aguasbonaerenses.com.ar"},
    {id: 4,img: 'assets/img/qliksense.jfif', text: 'QLIK',subtext:'',body:'',cols: 1, rows: 2,link:"http://"},
    {id: 5,img: 'assets/img/raet.jfif', text: 'RAET',subtext:'',body:'',cols: 1, rows: 2,link:"http://"},
    {id: 6,img: 'assets/img/outlook.jpg', text: 'OUTLOOK',subtext:'',body:'',cols: 1, rows: 2,link:"http://correo.aguasbonaerenses.com.ar"},
    {id: 7,img: 'assets/img/mesa.png', text: 'MESA DE ENTRADA',subtext:'',body:'',cols: 1, rows: 2, link:"http://172.27.104.193/tyr/main.aspx"},
{id:7,img: 'assets/img/mesa.png',text:"Mesa de Entradas",subtext:"Aplicación para la Gestión de Envíos y Recepción de Documentación",body:"<p>\n      Sistema de Gestión de Ordenes de Trabajo<br \/>\n      <br \/>\n<\/p>\n<br \/>\n<h3>Links del aplicativo:<\/h3>\n<mat-list>\n  <mat-list-item>\n    <mat-icon matListIcon>headset_mic<\/mat-icon>\n  <a href=\"http:\/\/172.27.104.193\/ot\/main.aspx\" target=\"_blank\">  <h3 matLine > OT - Produccion<\/h3><\/a>\n<\/mat-list-item>\n<mat-list-item>\n  <mat-icon matListIcon>headset_mic<\/mat-icon>\n<a href=\"http:\/\/172.27.104.187\/ot\/main.aspx\" target=\"_blank\">  <h3 matLine > OT - Capacitacion y Pruebas <\/h3><\/a>\n<\/mat-list-item>\n<mat-list-item>\n  <mat-icon matListIcon>attach_file<\/mat-icon>\n  <a href=\"assets\/files\/Manual_de_OT.pdf\" download><h3 matLine > Manual de OT <\/h3><\/a>\n<\/mat-list-item>\n<\/mat-list>",cols:1,rows:2,link:"http://172.27.104.193/tyr/main.aspx"},
    {id: 9,img: 'assets/img/verificacion.jpg', text: 'VERIFICACION',subtext:'',body:'',cols: 1, rows: 2,link:"http://capacitacion.aguasbonaerenses.com.ar"},
    {id: 10,img: 'assets/img/glpi.png', text: 'GLPI',subtext:'',body:'',cols: 1, rows: 2,link:"http://"},
    {id: 11,img: 'assets/img/omni.png', text: 'OMNICANALIDAD',subtext:'',body:'',cols: 1, rows: 2,link:"http://"},
    {id: 12,img: 'assets/img/totem.png', text: 'TOTEM',subtext:'',body:'',cols: 1, rows: 2,link:"http://correo.aguasbonaerenses.com.ar"},
    {id: 13,img: 'assets/img/logoai.png', text: 'PRESU_REG',subtext:'',body:'',cols: 1, rows: 2,link:"http://172.27.107.208/Intranet_sistemas/sis_administrativos/control_presupuestario/login.asp"},
    {id: 14,img: 'assets/img/logoai.png', text: 'PRESU_CTRAL',subtext:'',body:'',cols: 1, rows: 2,link:"http://172.27.107.208/Intranet_sistemas/sis_administrativos/control_presupuestario_HOF/login.asp"},
    {id: 15,img: 'assets/img/logoai.png', text: 'TABLERO_CTRL',subtext:'',body:'',cols: 1, rows: 2,link:"http://172.27.107.208/Intranet_sistemas/sis_administrativos/Login_General.asp?grupo=compras&querystring=&ret_page=%2FIntranet%5Fsistemas%2Fsis%5Fadministrativos%2Fcompras9%2Easp"},
    {id: 16,img: 'assets/img/logoai.png', text: 'NP_PENDIENTES',subtext:'',body:'',cols: 1, rows: 2,link:"http://172.27.107.208/Intranet_sistemas/sis_administrativos/Login_General.asp?grupo=compras&querystring=&ret_page=%2FIntranet%5Fsistemas%2Fsis%5Fadministrativos%2Fcompras2%2Easp"},
    {id: 17,img: 'assets/img/logoai.png', text: 'SC_PENDIENTES',subtext:'',body:'',cols: 1, rows: 2,link:"http://172.27.107.208/Intranet_sistemas/sis_administrativos/Login_General.asp?grupo=compras&querystring=&ret_page=%2FIntranet%5Fsistemas%2Fsis%5Fadministrativos%2FSolicitudPenBus%2Easp"},


  ];*/


constructor (private _app: AppsService){}

ngOnInit() {
  this._app.getaplicaciones().subscribe(data => this.apps=data);
  console.log(this.apps);
}


}
/*externalUrl(link:string){
  window.open(link);
}*/
