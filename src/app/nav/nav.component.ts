import { Component , OnInit} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {AuthService} from '../services/Auth.service';
import {TokenService} from '../services/token.service';



@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public loggedIn : boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private Auth : AuthService, private Token: TokenService, private breakpointObserver: BreakpointObserver) {}
public userDisplayName = '';
public UserNameDisplayname = '';
  ngOnInit(){
  this.Auth.authStatus.subscribe(value => this.loggedIn=value);
   this.userDisplayName = this.Token.getUser();
   this.UserNameDisplayname = this.Token.getUsername();
   console.log(this.UserNameDisplayname);


    }

}
