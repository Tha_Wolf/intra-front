import { Component, OnInit } from '@angular/core';
import {MatFormFieldModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {LoginService} from '../services/login.service';
import {TokenService} from '../services/token.service';
import {AuthService} from '../services/Auth.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = {
    username:null,
    password:null,
  }



  constructor(private Log:LoginService,
  private Token:TokenService,
private router: Router,
private Auth: AuthService, ) { }

  ngOnInit() {
  }
  login(){
  this.Log.login(this.form).subscribe(
    data => this.handleResponse(data),
    error => console.log(error)
  );
}

  handleResponse(data){
    this.Token.handle(data.access_token);
    this.Token.setUser(data.user);
    this.Token.setUsername(data.username);
    this.Auth.changeAuthStatus(true);
    this.router.navigateByUrl('/home');
  }


}
