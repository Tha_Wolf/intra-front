import { Component , OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LikesService} from '../services/likes.service';
import {TokenService} from '../services/token.service';
import{PostsService} from '../services/posts.service';
import {MatButtonModule} from '@angular/material';
import {MatIconModule} from '@angular/material';
import { map } from 'rxjs/operators';


export interface Publicacion {
  id: number;
  title: string;
  img:string;
  text: string;
  html: string;
  likeUsuario:string;
  habilitado:boolean;
}

export interface Likeado{
  id:number;
  username_id:string;
  post_id:string;
}

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})



export class Publicaciones implements OnInit{
  public form = {
    post_id:null,
    username_id:null,
  };
  likeados:Likeado[] = [];

  publicaciones: Publicacion[] = [/*
    {id:1,img:'assets/img/pub1.png',title:'Búsqueda Interna',text: 'Participamos a los empleados de Aguas Bonaerenses S.A. en nuestra búsqueda laboral para cubrir el puesto de  ANALISTA en el sector CATASTRO para desarrollar tareas en el EDIFICIO CENTRAL.',html:'<div><b>This text is bold</b> and this one is <i>italics</i></div>'},
    {id:2,img:'assets/img/pub2.png',title:'1° MAYO-DIA DEL TRABAJADOR',text: 'Feliz Dia Del Trabajador para todos los que trabajan día a día para mejorar el servicio de agua potable y cloacas de cada región.',html:'<div><b>This text is bold</b> and this one is <i>italics</i></div>'},
    {id:3,img:'assets/img/pub3.png',title:'Actualización Aplicativo JAVA',text: 'Con el objetivo de mejorar la calidad y tiempo de atencion, la simplificacion del trabajo para el operador y la apertura a nuevas tecnilogias, se realilzo la actualizacion y mejora de la version del aplicativo comercial JAVA y unificacion del entorno 4GL.',html:'<div><b>This text is bold</b> and this one is <i>italics</i></div>'},
*/
  ];



  publicacion: Publicacion = this.publicaciones[1];

  constructor(private route: ActivatedRoute, private Like:LikesService, private Token:TokenService, private Post:PostsService) {}

  darlike(id) {
    this.form.post_id = id;
    this.form.username_id = this.Token.getUsername();
    this.Like.create(this.form).subscribe(
      data => console.log(data),
      complete => this.ngOnInit(),
    );

  }

  gethabilitado(id){
    for (let likeado of this.likeados){
      if(id == +likeado.post_id)
      {
        return true;
      }
    }

  }




  ngOnInit() {
    this.form.username_id = this.Token.getUsername();
    this.Post.getpublicaciones().subscribe(data => this.publicaciones = (data),
    );
    this.Like.isHabilitado(this.form ).subscribe(data => this.likeados = (data));

    /*this.publicaciones.forEach((p: Publicacion) => {
        if (p.id == this.route.snapshot.params.id) {
          this.publicacion = p;
        }
      });*/


  }
}
