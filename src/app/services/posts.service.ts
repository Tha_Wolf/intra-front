import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Publicacion} from '../post/post.component';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private httpclient:HttpClient){ }
  getpublicaciones():Observable<Publicacion[]>{
    return this.httpclient.get<Publicacion[]>("http://intranet.aguasbonaerenses.com.ar/back//api/posts");
  }
}
