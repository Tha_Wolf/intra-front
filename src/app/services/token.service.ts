import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  handle(token){
    this.set(token);
  }

  set(token){
    localStorage.setItem('token',token);
  }

  get(){
    return localStorage.getItem('token');
  }

  remove(){
    localStorage.removeItem('token');
  }

  isValid(){
    const token = this.get();
    if(token){
      const payload= this.payload(token);
      if(payload){
        return payload.iss === 'http://intranet.aguasbonaerenses.com.ar/back//api/login' ? true : false;
      }

    }
    return false;
  }

  payload(token){
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload){
  return JSON.parse(atob(payload));
  }

  loggedIn(){
    return this.isValid();
  }

 setUser(username){
   localStorage.setItem('loggedUser',username);
 }

 setUsername(username){
   localStorage.setItem('loggedUsername',username);
 }
  userLogged(){
    if (localStorage.getItem('loggedUser')){
      return localStorage.getItem('loggedUser');
    };
  }

  userNameLogged(){
    if(localStorage.getItem('loggedUsername')){
      return localStorage.getItem('loggedUsername');
    }
  }

  getUser(){
    return this.userLogged();
  }

  getUsername(){
    return this.userNameLogged();
  }
}
