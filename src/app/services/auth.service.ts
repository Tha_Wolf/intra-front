import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {TokenService} from './token.service';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(this.Token.loggedIn());


  authStatus = this.loggedIn.asObservable();




  changeAuthStatus(value:boolean){
    this.loggedIn.next(value);

  }

  userLogged(){
      return localStorage.getItem('user');
  }

  constructor(private Token : TokenService) { }
}
