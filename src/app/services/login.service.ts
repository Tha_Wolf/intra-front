import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  login(data){
   return this.http.post('http://intranet.aguasbonaerenses.com.ar/back//api/login',data)
  }
}
