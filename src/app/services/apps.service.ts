import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {App} from '../apps-lista/apps-lista.component';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppsService {

  constructor(private httpclient:HttpClient){ }
  getaplicaciones():Observable<App[]>{
    return this.httpclient.get<App[]>("http://intranet.aguasbonaerenses.com.ar/back/api/apps");
  }
}
