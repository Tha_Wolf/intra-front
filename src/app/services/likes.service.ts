import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Likeado} from '../post/post.component';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LikesService {


  constructor(private http:HttpClient) { }

  create(data){
   return this.http.post('http://intranet.aguasbonaerenses.com.ar/back//api/like',data)
  }

  count(data){
    return this.http.post('http://intranet.aguasbonaerenses.com.ar/back//api/likecount',data)
  }


  isHabilitado(data):Observable<Likeado[]>{
    return this.http.post<Likeado[]>("http://intranet.aguasbonaerenses.com.ar/back//api/likehabilitado",data);
  }
}
