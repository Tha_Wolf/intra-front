import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DirectorioTelefonico} from '../directorio/directorio.component';
import {Observable} from 'rxjs';

@Injectable()
export class DirectorioTelefonicoService
{
  constructor(private httpclient:HttpClient){ }
  gettelefonos():Observable<DirectorioTelefonico[]>{
    return this.httpclient.get<DirectorioTelefonico[]>("http://intranet.aguasbonaerenses.com.ar/back//api/DirectorioTelefonico");
  }
}
